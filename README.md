# Inpost (Magento 2 module)

This module provides integration with InPost, Polish shipping carrier.

## Installation

Install the latest version with

```bash
$ composer config repositories.inpost git git@bitbucket.org:zefiryn/m2-inpost.git
$ composer require zefiryn/m2-inpost ^1.0
```

## Implemented functionalities

- Fetch machines based on address
- Add selection form into checkout
- Use Google Map to visualize machines location
- Use OpenstreetMap to visualize machines location
- Show machines select box in admin panel order creation

## Not implemented functionalities

- Create shipping label via API call
- Cache machine response by zipcode

## Configuration

- `Selection Type` allows to select type of machine visualization. There are 4 options possible:
    * `Select box` - simple select box with machines
    * `Google Maps` - use google maps to display local area with machines marked on it. This options requires to provide API key for google map service.
    * `OpenStreetMaps (OpenLayers v3)` - use OpenStreetMap data for displaying the map with OpenLayers v3 as a tile provider 
    * `OpenStreetMaps (Leaflet)` - use OpenStreetMap data for displaying the map with Leaflet library. This option requires additional `Tile Provider` to be selected which defines look of the tiles.

## Known issues

- `select box` on frontend is not working
- open layers does not show details information when selecting the machine
- open layers does not group when zooming