# Module version changelog

1.0.1 (January 10, 2018)
--------------------

- Fix saving machine in admin created order

1.0.0 (January 10, 2018)
--------------------

- Basic functionality of the module