<?php

namespace Zefiryn\InPost\Model\Config\Source;

/**
 * Class Selection
 *
 * @package Zefiryn\InPost\Model\Config\Source
 * @author Artur Jewuła <zefiryn@jewula.net>
 */
class Selection implements \Magento\Framework\Option\ArrayInterface
{
    const CONFIG_PATH = 'carriers/inpost/selection_type';

    const SIMPLE = 0;
    const GOOGLE = 1;
    const OPEN_LAYERS = 2;
    const LEAFLET = 3;

    const SIMPLE_LABEL = 'select';
    const GOOGLE_LABEL = 'google';
    const OPEN_LAYERS_LABEL = 'open_layers';
    const LEAFLET_LABEL = 'leaflet';

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => self::SIMPLE, 'label' => __('Select box')],
            ['value' => self::GOOGLE, 'label' => __('Google Maps')],
            ['value' => self::OPEN_LAYERS, 'label' => __('OpenStreetMaps (OpenLayers v3)')],
            ['value' => self::LEAFLET, 'label' => __('OpenStreetMaps (Leaflet)')]
        ];
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        return [
            self::SIMPLE => __('Select box'),
            self::GOOGLE => __('Google Maps'),
            self::OPEN_LAYERS => __('OpenStreetMaps (OpenLayers v3)'),
            self::LEAFLET => __('OpenStreetMaps (Leaflet)')
        ];
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toLabelArray()
    {
        return [
            self::SIMPLE => self::SIMPLE_LABEL,
            self::GOOGLE => self::GOOGLE_LABEL,
            self::OPEN_LAYERS => self::OPEN_LAYERS_LABEL,
            self::LEAFLET => self::LEAFLET_LABEL
        ];
    }
}
