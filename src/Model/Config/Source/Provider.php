<?php

namespace Zefiryn\InPost\Model\Config\Source;

/**
 * Leaflet Providers
 * @package Zefiryn\InPost\Model\Config\Source
 * @author Artur Jewuła <zefiryn@jewula.net>
 */
class Provider
{
    const OPEN_STREET_MAP_BW = 'OpenStreetMap.BlackAndWhite';
    const OPEN_STREET_MAP_HOT = 'OpenStreetMap.HOT';
    const HYDDA_FULL = 'Hydda.Full';
    const ESRI_WORLD = 'Esri.WorldStreetMap';
    const ESRI_WORLD_TOPO = 'Esri.WorldTopoMap';
    const MTB_MAP = 'MtbMap';
    const CARTO_DB = 'CartoDB';
    const HIKE_BIKE = 'HikeBike';

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return [
            ['value' => self::OPEN_STREET_MAP_BW , 'label' => __('OpenStreetMap (Black and White)')],
            ['value' => self::OPEN_STREET_MAP_HOT , 'label' => __('OpenStreetMap (HOT)')],
            ['value' => self::HYDDA_FULL , 'label' => __('Hydda')],
            ['value' => self::ESRI_WORLD , 'label' => __('ESRI (World)')],
            ['value' => self::ESRI_WORLD_TOPO , 'label' => __('ESRI (World Topo Map)')],
            ['value' => self::MTB_MAP , 'label' => __('MtbMap')],
            ['value' => self::CARTO_DB , 'label' => __('CartoDB')],
            ['value' => self::HIKE_BIKE , 'label' => __('HikeBike')],
        ];
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        return [
            self::OPEN_STREET_MAP_BW => __('OpenStreetMap (Black and White)'),
            self::OPEN_STREET_MAP_HOT => __('OpenStreetMap (HOT)'),
            self::HYDDA_FULL => __('Hydda'),
            self::ESRI_WORLD => __('ESRI (World)'),
            self::ESRI_WORLD_TOPO => __('ESRI (World Topo Map)'),
            self::MTB_MAP => __('MtbMap'),
            self::CARTO_DB => __('CartoDB'),
            self::HIKE_BIKE => __('HikeBike'),
        ];
    }

}