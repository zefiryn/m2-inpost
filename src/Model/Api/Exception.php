<?php

namespace Zefiryn\InPost\Model\Api;

use Magento\Framework\Exception\LocalizedException;

/**
 * Class Exception
 * @package Zefiryn\InPost\Model\Api
 * @author Artur Jewuła <zefiryn@jewula.net>
 */
class Exception extends LocalizedException
{

}