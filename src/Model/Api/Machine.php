<?php

namespace Zefiryn\InPost\Model\Api;

use Magento\Framework\DataObject;
use Zefiryn\InPost\Api\Data\MachineInterface;

/**
 * Class Machine
 * @package Zefiryn\InPost\Model\Api
 * @author Artur Jewuła <zefiryn@jewula.net>
 */
class Machine extends DataObject implements MachineInterface
{
    /**
     * @return string
     */
    public function getName()
    {
        return (string)$this->_getData(self::NAME);
    }

    /**
     * @return string
     */
    public function getLocation()
    {
        return trim((string)$this->getStreet() . ' '. (string)$this->getBuildingNumber());
    }

    /**
     * @return string
     */
    public function getPostcode()
    {
        return (string)$this->_getData(self::POSTCODE);
    }

    /**
     * @return string
     */
    public function getStreet()
    {
        return (string)$this->_getData(self::STREET);
    }

    /**
     * @return string
     */
    public function getBuildingNumber()
    {
        return (string)$this->_getData(self::BUILDING_NUMBER);
    }

    /**
     * @return string
     */
    public function getTown()
    {
        return (string)$this->_getData(self::TOWN);
    }

    /**
     * @return float
     */
    public function getLatitude()
    {
        return (float)$this->_getData(self::LATITUDE);
    }

    /**
     * @return float
     */
    public function getLongitude()
    {
        return (float)$this->_getData(self::LONGITUDE);
    }

    /**
     * @return float
     */
    public function getDistance()
    {
        return (float)$this->_getData(self::DISTANCE);
    }

    /**
     * @return string
     */
    public function getPaymentType()
    {
        return (int)$this->_getData(self::PAYMENT_TYPE);
    }

    /**
     * @return string
     */
    public function getLocationDescription()
    {
        return (string)$this->_getData(self::LOCATION_DESCRIPTION);
    }
}