<?php

namespace Zefiryn\InPost\Model\Api;

/**
 * API Response Interface
 * @package Zefiryn\InPost\Model\Api
 * @author Artur Jewuła <zefiryn@jewula.net>
 */
interface ResponseInterface
{
    /**
     * Parse response from API
     *
     * @param $operation
     * @param $result
     * @return mixed
     */
    public function parseResponse($operation, $result);

}