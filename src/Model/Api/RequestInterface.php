<?php

namespace Zefiryn\InPost\Model\Api;

/**
 * API Request Interface
 * @package Zefiryn\InPost\Model\Api
 * @author Artur Jewuła <zefiryn@jewula.net>
 */
interface RequestInterface
{
    /**
     * Make request call to API for specific operation
     *
     * @param string $operation
     * @param array $params
     * @return mixed
     */
    public function call($operation, $params);

}