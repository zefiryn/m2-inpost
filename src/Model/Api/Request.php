<?php

namespace Zefiryn\InPost\Model\Api;

use Magento\Framework\HTTP\ClientFactory;
use Magento\Framework\HTTP\Client\Curl;
use Magento\Framework\Event\ManagerInterface;

/**
 * Class Request
 *
 * @package Zefiryn\InPost\Model\Api
 * @author Artur Jewuła <zefiryn@jewula.net>
 */
class Request extends AbstractApi implements RequestInterface
{
    /**
     * API endpoint url
     */
    const API_ENPOINT = 'http://api.paczkomaty.pl/';

    /**
     * @var ClientFactory
     */
    protected $clientFactory;

    /**
     * @var ManagerInterface
     */
    protected $_eventDispatcher;

    /**
     * Request constructor.
     * @param ClientFactory $clientFactory
     * @param ManagerInterface $eventDispatcher
     * @param array $data
     */
    public function __construct(ClientFactory $clientFactory, ManagerInterface $eventDispatcher, array $data = [])
    {
        $this->clientFactory = $clientFactory;
        $this->_eventDispatcher = $eventDispatcher;
        parent::__construct($data);
    }

    /**
     * Perform API request
     *
     * @param string $operation
     * @param array $params
     * @return string
     * @throws Exception
     */
    public function call($operation, $params)
    {
        $data = $this->callOperationHandleMethod($operation, $params);
        /** @var Curl $client */
        $client = $this->clientFactory->create();
        $client->setTimeout($this->getRequestTimeout());
        $uri = self::API_ENPOINT.'?do='.$operation;
        if ($data['method'] == \Zend_Http_Client::GET) {
            $uri .= '&'.$this->buildQueryString($data);
        }
        else {
            $params = array_key_exists('params', $data) ? $data['params'] : array();
        }

        $this->_eventDispatcher->dispatch('inpost_api_call_' . $operation . '_before', array('client' => $client, 'params' => $data));
        $method = $data['method'] == \Zend_Http_Client::GET ? 'get' : 'post';
        $client->$method($uri, $params);
        return $client->getBody();
    }

    /**
     * @see RequestInterface::prepareListmachinesCsv
     * @param array $params
     * @return array
     */
    public function prepareListmachinesCsv($params = array())
    {
        $data = [
            'method' => \Zend_Http_Client::GET,
            'params' => ['paymentavailable' => 't', 'pickuppoint' => 'f']
        ];
        return $data;
    }

    /**
     * @see RequestInterface::prepareListmachinesXml
     * @param array $params
     * @return array
     */
    public function prepareListmachinesXml($params = array())
    {
        $data = [
            'method' => \Zend_Http_Client::GET,
            'params' => ['paymentavailable' => 't', 'pickuppoint' => 'f']
        ];
        return $data;
    }

    /**
     * Prepare data for findenearestmachines call
     * @param array $params
     * @return array
     */
    public function prepareFindnearestmachines($params)
    {
        return [
            'method' => \Zend_Http_Client::GET,
            'params' => [
                'postcode' => $params['postcode'],
                'limit' => array_key_exists('limit', $params) ? $params['limit'] : 50
            ]
        ];
    }

    /**
     * Prepare data for findnearestmachnes_csv call
     *
     * @param array $params
     * @return array
     */
    public function prepareFindnearestmachinesCsv($params)
    {
        return [
            'method' => \Zend_Http_Client::GET,
            'params' => ['postcode' => $params['postcode']]
        ];
    }

    /**
     * Prepare data for pricelist call
     * @param array $params
     * @return mixed
     */
    public function preparePricelist($params)
    {
        return [];
    }

    /**
     * Prepare data for checking package status
     *
     * @param array $params
     * @return array
     */
    public function prepareGetPackStatus($params)
    {
        return [
            'method' => \Zend_Http_Client::GET,
            'params' => ['packcode' => $params['packcode']]
        ];
    }

    /**
     * Add parameters to the request
     *
     * @param array $data
     * @return string
     */
    protected function buildQueryString($data)
    {
        $queryString = '';
        if (array_key_exists('params', $data)) {
            $queryString = http_build_query($data['params']);
        }
        return $queryString;
    }
}