<?php

namespace Zefiryn\InPost\Model\Api;

use Magento\Framework\DataObject;
use Magento\Framework\Phrase;

/**
 * Class AbstractApi
 *
 * @package Zefiryn\InPost\Model\Api
 * @author Artur Jewuła <zefiryn@jewula.net>
 */
abstract class AbstractApi extends DataObject
{
    /**
     * Build method name.
     * $operation is a string of request command, ie. listmachines_csv
     * this string is replaced to camelCase with `prepare` prefix
     * ie prepareListmachinesCsv
     *
     * @param $operation
     * @return string
     */
    protected function prepareMethodName($operation)
    {
        return 'prepare' . str_replace(' ', null, ucwords(str_replace('_', ' ', $operation)));
    }

    /**
     * Call method that handle operation preparing and parsing
     *
     * @param $operation
     * @param mixed $params
     * @return mixed
     * @throws Exception
     */
    protected function callOperationHandleMethod($operation, $params)
    {
        $method = $this->prepareMethodName($operation);
        if (!method_exists($this, $method)) {
            throw new Exception(new Phrase('Operation not supported'));
        }

        return $this->$method($params);
    }

    /**
     * @todo replace with config value
     * @return int
     */
    protected function getRequestTimeout()
    {
        return 30;
    }
}