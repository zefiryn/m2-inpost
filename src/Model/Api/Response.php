<?php

namespace Zefiryn\InPost\Model\Api;

use Magento\Framework\Phrase;

/**
 * Class Response
 * @package Zefiryn\InPost\Model\Api
 * @author Artur Jewuła <zefiryn@jewula.net>
 */
class Response extends AbstractApi implements ResponseInterface
{
    /**
     * @var MachineFactory
     */
    protected $_machineFactory;

    /**
     * Response constructor.
     * @param MachineFactory $machineFactory
     * @param array $data
     */
    public function __construct(MachineFactory $machineFactory, array $data = [])
    {
        $this->_machineFactory = $machineFactory;
        parent::__construct($data);
    }

    /**
     * @param string $operation
     * @param mixed $result
     * @return mixed|void
     * @throws Exception
     */
    public function parseResponse($operation, $result)
    {
        $method = $this->prepareMethodName($operation);
        if (!method_exists($this, $method)) {
            throw new Exception(new Phrase('Unknown operation'));
        }
        $parsedResponse = $this->$method($result);

        return $parsedResponse;
    }

    /**
     * Parse response from findnearestmachines call
     *
     * @param $result
     * @return array
     */
    public function prepareFindnearestmachines($result)
    {
        $machines = [];
        $xml = simplexml_load_string($result);
        if ($xml->machine) {
            foreach($xml->machine as $machine) {
                /** @var Machine $machineObj */
                $machineObj = $this->_machineFactory->create(['data' => (array)$machine]);
                $machines[] = $machineObj;
            }
        }

        return $machines;
    }

    /**
     * @param $result
     * @return null|string
     */
    public function prepareGetpackstatus($result)
    {
        $resultResponse = simplexml_load_string($result);
        if ($resultResponse) {
            return (string) $resultResponse->status;
        }

        return null;
    }
}