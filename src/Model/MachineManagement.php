<?php

namespace Zefiryn\InPost\Model;

use Zefiryn\InPost\Api\Data\MachineInterface;
use Zefiryn\InPost\Api\MachineManagementInterface;

/**
 * Class MachineManagement
 *
 * @package Zefiryn\InPost\Model
 * @author Artur Jewuła <zefiryn@jewula.net>
 */
class MachineManagement implements MachineManagementInterface
{
    /**
     * @var Api
     */
    protected $_api;

    /**
     * MachineManagement constructor.
     *
     * @param Api $api
     */
    public function __construct(Api $api)
    {
        $this->_api = $api;
    }

    /**
     * Get machines for the given postcode
     *
     * @param string $postcode
     * @param int $limit
     * @return MachineInterface[]
     */
    public function fetchNearestMachines($postcode, $limit)
    {
        $result = $this->_api->performRequest('findnearestmachines', ['postcode' => $postcode, 'limit' => $limit]);
        return $result;
    }
}