<?php

namespace Zefiryn\InPost\Model;

use Magento\Framework\DataObject;
use Magento\Framework\Phrase;
use Zefiryn\InPost\Model\Api\RequestInterface;
use Zefiryn\InPost\Model\Api\ResponseInterface;
use Zefiryn\InPost\Api\ApiInterface;
use Zefiryn\InPost\Model\Api\Exception;

/**
 * API for InPost Paczkomaty
 *
 * @package Zefiryn\InPost\Model
 * @author Artur Jewuła <zefiryn@jewula.net>
 */
class Api extends DataObject implements ApiInterface
{
    /**
     * @var RequestInterface
     */
    protected $_request;

    /**
     * @var ResponseInterface
     */
    protected $_response;

    public function __construct(RequestInterface $request, ResponseInterface $response, array $data = [])
    {
        $this->_request = $request;
        $this->_response = $response;
        parent::__construct($data);
    }

    /**
     * Make request call to InPost API
     *
     * @param string $operation
     * @param array $params
     * @return mixed
     * @throws Exception
     */
    public function performRequest($operation = null, $params = array())
    {
        if (null === $operation) {
            throw new Exception(new Phrase('Missing request command.'));
        }

        $response = $this->_request->call($operation, $params);
        return $this->_response->parseResponse($operation, $response);
    }
}