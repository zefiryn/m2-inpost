define([
    'jquery'
], function($) {
    return {
        element: null,

        'Zefiryn_InPost/js/form': function(object, element) {
            this.element = $(element);
            this._init();
        },

        _init: function() {
            var shippingMethod = $('#order-shipping_method input[name="order[shipping_method]"]:checked');
            if (shippingMethod.length > 0 && shippingMethod.val() == 'inpost_inpost') {
                this.element.show();
            }
            else {
                this.element.hide();
            }
        }
    };
});