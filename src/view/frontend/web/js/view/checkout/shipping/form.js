define([
    'jquery',
    'ko',
    'uiComponent',
    'Magento_Checkout/js/model/quote',
    'Magento_Checkout/js/model/shipping-service',
    'Zefiryn_InPost/js/view/checkout/shipping/model/map',
    'Zefiryn_InPost/js/view/checkout/shipping/machine-service',
    'Magento_Checkout/js/model/shipping-rate-registry',
    'mage/translate',
], function ($, ko, Component, quote, shippingService, map, machineService, rateRegistry, t) {
    'use strict';

    return Component.extend({
        defaults: {
            template: 'Zefiryn_InPost/checkout/shipping/form',
            maps: null,
            leafletProvider: null,
            reloading: false
        },

        initialize: function (config) {
            this.machines = ko.observableArray();
            this.selectedMachine = ko.observable();
            return this._super();
        },

        initObservable: function () {
            this._super();

            this.selectedMachineInfo = ko.computed(function() {
                var selected = this.selectedMachine();
                var machines = $.grep(this.machines(), function(e){ return e.name == selected; });
                var machine = machines.length > 0 ? machines[0] : null;
                return machine ? selected + ': ' + machine.location : '';
            }, this);

            this.showMachineSelection = ko.computed(function() {
                return this.machines().length != 0
            }, this);

            this.selectedMethod = ko.computed(function() {
                var method = quote.shippingMethod();
                var selectedMethod = method != null ? method.carrier_code + '_' + method.method_code : null;
                return selectedMethod;
            }, this);

            this.machineLimit = ko.observable(20);
            this.limit = ko.computed(function() {
                return this.machineLimit();
            }, this);

            quote.shippingAddress.subscribe(function(newAddress) {
                this.reloadMachines();
            }, this);

            quote.shippingMethod.subscribe(function(method) {
                var selectedMethod = method != null ? method.carrier_code + '_' + method.method_code : null;
                if (selectedMethod == 'inpost_inpost' && window.map) {
                    map.reinitMap();
                }
                else if (selectedMethod == 'inpost_inpost') {
                    if ($('#machine-map').length > 0) {
                        this.initMap();
                    }
                }
            }, this);

            this.selectedMachine.subscribe(function(machine) {
                var address = quote.shippingAddress();
                if (address.extensionAttributes == undefined) {
                    address.extensionAttributes = {};
                }
                address.extensionAttributes.inpost_machine = machine;

                //update method title to include selected machine
                var cache = rateRegistry.get(address.getCacheKey());
                if (!cache) {
                    return;
                }
                var inpostRatesIdx = cache.findIndex(function(item) {
                    return item.carrier_code === 'inpost';
                });
                var title = cache[inpostRatesIdx].method_title;
                var previousMachine = title.lastIndexOf(' ');
                if (previousMachine > -1) {
                    title =     title.substring(0, previousMachine);
                }
                cache[inpostRatesIdx].method_title = title + ' (' + machine + ')';
                rateRegistry.set(address.getCacheKey(), cache);
            });


            return this;
        },

        setMachinesList: function(list) {
            this.machines(list);
            this.refreshMap();
            this.reloading = false;
        },

        addMoreMachines: function() {
            this.machineLimit(this.machineLimit() + 20);
            this.reloadMachines();
        },

        reloadMachines: function() {
            if (quote.shippingAddress().postcode) {
                machineService.getMachineList(quote.shippingAddress(), this);
                this.reloading = true;
            }
        },

        getMachine: function() {
            var machine;
            if (this.selectedMachine()) {
                for (var i in this.machines()) {
                    var m = this.machines()[i];
                    if (m.name == this.selectedMachine()) {
                        machine = m;
                    }
                }
            }
            else if (!this.reloading) {
                if (this.machines().length == 0) {
                    this.reloadMachines();
                }
                machine = this.machines()[0];
            }

            return machine;
        },

        initMap: function() {
            var startMachine = this.getMachine();
            map.initMap(startMachine, this);
        },

        refreshMap: function () {
            if (!window.map) {
                this.initMap();
            }
            if (window.map) {
                map.reloadMarkers(this.machines());
            }
        }
    });
});