define(
    [
        'Zefiryn_InPost/js/view/checkout/shipping/model/resource-url-manager',
        'Magento_Checkout/js/model/quote',
        'Magento_Customer/js/model/customer',
        'mage/storage',
        'Magento_Checkout/js/model/shipping-service',
        'Zefiryn_InPost/js/view/checkout/shipping/model/inpost-machine-registry',
        'Magento_Checkout/js/model/error-processor'
    ],
    function (resourceUrlManager, quote, customer, storage, shippingService, machineRegistry, errorProcessor) {
        'use strict';

        return {
            /**
             * Get nearest machine list for specified address
             * @param {Object} address
             */
            getMachineList: function (address, form) {
                shippingService.isLoading(true);
                var cacheKey = address.postcode + '_' + form.machineLimit(),
                    cache = machineRegistry.get(cacheKey),
                    serviceUrl = resourceUrlManager.getUrlForInPostMachineList(quote, form.machineLimit());

                if (cache) {
                    form.setMachinesList(cache);
                    shippingService.isLoading(false);
                } else {
                    storage.get(
                        serviceUrl, false
                    ).done(
                        function (result) {
                            machineRegistry.set(cacheKey, result);
                            form.setMachinesList(result);
                        }
                    ).fail(
                        function (response) {
                            errorProcessor.process(response);
                        }
                    ).always(
                        function () {
                            shippingService.isLoading(false);
                        }
                    );
                }
            }
        };
    }
);
