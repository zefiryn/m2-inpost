define([
    'jquery',
    'leaflet',
    'googleMarkerCluster',
    'leafletMarkerCluster',
    'leafletProviders',
    'leafletMarkerClusterGroup',
    'leafletMarkerDistanceGrid',
    'Zefiryn_InPost/js/lib/leaflet-markercluster/MarkerClusterGroup.Refresh',
    'Zefiryn_InPost/js/lib/leaflet-markercluster/MarkerCluster.QuickHull',
    'Zefiryn_InPost/js/lib/leaflet-markercluster/MarkerCluster.Spiderfier',
    'leafletMarkerOpacity'
], function ($) {
    'use strict';

    return {
        selectedMap: null,
        leafletProvider: null,
        form: null,

        initMap: function(startMachine, form) {
            this.selectedMap = form.maps;
            this.leafletProvider = form.leafletProvider;
            this.form = form;

            if (!startMachine) {
                return;
            }

            if (this.selectedMap == 'google') {
                this.initGoogleMap(startMachine);
            }
            else if (this.selectedMap == 'open_layers') {
                this.initOpenLayersMap(startMachine);
            }
            else if (this.selectedMap == 'leaflet') {
                this.initLeafletMap(startMachine);
            }
        },

        reinitMap: function() {
            if (this.selectedMap == 'google') {
                google.maps.event.trigger(window.map, 'resize');
            }
            else if (this.selectedMap == 'open_layers') {

            }
            else if (this.selectedMap == 'leaflet') {
                window.map.invalidateSize();
            }
        },

        reloadMarkers: function(machineList) {
            if (this.selectedMap == 'google') {
                this.reloadGoogleMarkers(machineList);
            }
            else if (this.selectedMap == 'open_layers') {
                this.reloadOpenLayersMarkers(machineList);
            }
            else if (this.selectedMap == 'leaflet') {
                this.reloadLeafletMarkers(machineList);
            }
        },

        prepareMachinePopup: function(machine) {
            var machineDescription = machine.location_description.split(',').filter(Boolean).join(',<br />');
            var content = '<strong>' + machineDescription + '</strong><br />' + machine.location + '<br />' + machine.postcode + ' ' + machine.town;

            return content;
        },

        /**
         * LEAFLET
         */

        leafletMarkers: null,

        leafletIcon: null,

        initLeafletMap: function(startMachine) {
            if (!window.map) {
                this.leafletIcon = window.L.icon({
                    iconUrl: requirejs.s.contexts._.config.baseUrl + 'Zefiryn_InPost/images/leaflet/marker-icon.png',
                    iconSize: [25, 41],
                    iconAnchor: [13, 39],
                    popupAnchor: [0, -40]
                });
                window.map = window.L.map('machine-map', {maxZoom: 19}).setView([startMachine.latitude, startMachine.longitude], 15);
                window.L.tileLayer.provider(this.leafletProvider).addTo(window.map);
            }
        },

        reloadLeafletMarkers: function(machineList) {
            var self = this;
            this.clearLeafletMarkers();
            this.leafletMarkers = window.L.markerClusterGroup();
            for (var i in machineList) {
                var machine = machineList[i];
                var marker = window.L.marker([machine.latitude, machine.longitude], {icon: this.leafletIcon, machineName: machine.name });
                marker.bindPopup(this.prepareMachinePopup(machine));
                marker.on('click', function() {
                    self.form.selectedMachine(this.options.machineName);
                });
                this.leafletMarkers.addLayer(marker);
            }
            map.addLayer(this.leafletMarkers);
        },

        clearLeafletMarkers: function() {
            if (this.leafletMarkers) {
                this.leafletMarkers.clearLayers();
            }
        },

        /**
         * OPEN LAYERS
         */
        initOpenLayersMap: function(startMachine) {
            if (!window.map) {
                window.map = new ol.Map({
                    layers: [
                        new ol.layer.Tile({source: new ol.source.OSM()})
                    ],
                    view: new ol.View({
                        center: ol.proj.transform([startMachine.longitude, startMachine.latitude], 'EPSG:4326', 'EPSG:3857'),
                        zoom: 15
                    }),
                    target: 'machine-map'
                });
            }
        },

        reloadOpenLayersMarkers: function(machineList) {
            this.clearOpenLayersMarkers();
            for (var i in machineList) {
                var image = requirejs.s.contexts._.config.baseUrl + 'Zefiryn_InPost/images/open-layers/pin.png';
                var machine = machineList[i];
                var pinElement = $('<img src="'+ image +'" />').data('machine', machine.name);
                var marker = new ol.Overlay({
                    position: ol.proj.transform([machine.longitude, machine.latitude], 'EPSG:4326', 'EPSG:3857'),
                    positioning: 'bottom-center',
                    element: pinElement[0]
                });
                pinElement.on('click', function(e) {
                    this.form.selectedMachine($(e.target).data('machine'));
                }.bind(this));
                map.addOverlay(marker);
            }

        },

        clearOpenLayersMarkers: function() {
            if (window.map.getOverlays().getLength() > 0) {
                window.map.getOverlays().clear();
            }
        },

        /**
         *  GOOGLE MAPS METHODS
         */

        googleMarkers: [],

        googleMarkerCluster: null,

        selectedMarker: null,

        initGoogleMap: function(startMachine) {
            MarkerClusterer.prototype.MARKER_CLUSTER_IMAGE_PATH_ = requirejs.s.contexts._.config.baseUrl + 'Zefiryn_InPost/images/open-layers/m';
            var mapDiv = document.getElementById('machine-map');
            if (!window.map) {
                window.map = new google.maps.Map(mapDiv, {
                    center: {lat: startMachine.latitude, lng: startMachine.longitude},
                    zoom: 15
                });
            }
        },

        reloadGoogleMarkers: function(machineList) {
            var self = this;
            this.clearGoogleMarkers();
            for (var i in machineList) {
                var machine = machineList[i];
                var marker = new google.maps.Marker({
                    position: {lat: machine.latitude, lng: machine.longitude},
                    map: window.map,
                    machineName: machine.name,
                    title: machine.location + ' (' + machine.name +')',
                    infoWindow: new google.maps.InfoWindow({
                        content: self.prepareMachinePopup(machine)
                    })
                });
                this.googleMarkers.push(marker);
                marker.addListener('click', function() {
                    if (self.selectedMarker) {
                        self.selectedMarker.infoWindow.close();
                    }
                    window.map.setCenter(this.getPosition());
                    self.form.selectedMachine(this.machineName);
                    this.infoWindow.open(window.map, this);
                    self.selectedMarker = this;
                });
            }
            this.googleMarkerCluster = new MarkerClusterer(window.map, this.googleMarkers);

        },

        clearGoogleMarkers: function() {
            for (var i in this.googleMarkers) {
                this.googleMarkers[i].setMap(null);
            }
            this.googleMarkers = [];
            if (this.googleMarkerCluster) {
                this.googleMarkerCluster.clearMarkers();
            }
        }
    };
});