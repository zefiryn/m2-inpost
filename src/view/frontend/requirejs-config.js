var config = {
    map: {
        '*': {
            leaflet: 'Zefiryn_InPost/js/lib/leaflet',
            googleMarkerCluster: 'Zefiryn_InPost/js/lib/markercluster',
            leafletProviders: 'Zefiryn_InPost/js/lib/leaflet-providers',
            leafletMarkerCluster: 'Zefiryn_InPost/js/lib/leaflet-markercluster/MarkerCluster',
            leafletMarkerClusterGroup: 'Zefiryn_InPost/js/lib/leaflet-markercluster/MarkerClusterGroup',
            leafletMarkerDistanceGrid: 'Zefiryn_InPost/js/lib/leaflet-markercluster/DistanceGrid',
            leafletMarkerOpacity: 'Zefiryn_InPost/js/lib/leaflet-markercluster/MarkerOpacity'
        }
    },
    shim: {
        "Zefiryn_InPost/js/lib/leaflet-markercluster/MarkerClusterGroup.Refresh":  ['leafletMarkerClusterGroup'],
        "Zefiryn_InPost/js/lib/leaflet-markercluster/MarkerCluster.QuickHull": ['leafletMarkerClusterGroup'],
        "Zefiryn_InPost/js/lib/leaflet-markercluster/MarkerCluster.Spiderfier": ['leafletMarkerClusterGroup'],
    }
};