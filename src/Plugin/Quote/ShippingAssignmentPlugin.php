<?php

namespace Zefiryn\InPost\Plugin\Quote;

use Magento\Quote\Api\Data\AddressInterface;
use Magento\Quote\Api\Data\ShippingInterface;
use Magento\Quote\Model\ShippingAssignment;
use Zefiryn\InPost\Model\Carrier;

/**
 * ShippingAssignmentPlugin
 * @package Zefiryn\InPost\Plugin\Quote
 * @author Artur Jewuła <zefiryn@jewula.net>
 */
class ShippingAssignmentPlugin
{
    /**
     * Hook into setShipping.
     *
     * @param ShippingAssignment $subject
     * @param ShippingInterface $value
     * @return Address
     */
    public function beforeSetShipping(ShippingAssignment $subject, ShippingInterface $value)
    {
        $method = $value->getMethod();
        /** @var AddressInterface $address */
        $address = $value->getAddress();
        if ($method === Carrier::CARRIER_CODE.'_'.Carrier::METHOD_CODE
            && $address->getExtensionAttributes()
            && $address->getExtensionAttributes()->getInpostMachine()
        ) {
            $address->setInpostMachine($address->getExtensionAttributes()->getInpostMachine());
        }
        elseif (
            $method !== Carrier::CARRIER_CODE.'_'.Carrier::METHOD_CODE
        ) {
            //reset inpost machine when changing shipping method
            $address->setInpostMachine(null);
        }
        return [$value];
    }
}