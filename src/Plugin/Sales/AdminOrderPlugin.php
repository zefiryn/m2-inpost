<?php
namespace Zefiryn\InPost\Plugin\Sales;

use Magento\Sales\Model\AdminOrder\Create;
use Zefiryn\InPost\Model\Carrier;

class AdminOrderPlugin
{
    /** @var string */
    protected $selectedMachine;

    /**
     * Get selected machine from POST data
     *
     * @param Create $subject
     * @param array $data
     * @return array
     */
    public function beforeImportPostData(Create $subject, $data)
    {
        if (is_array($data) && array_key_exists('inpost_machine', $data)) {
            $this->selectedMachine = $data['inpost_machine'];
        }

        return [$data];
    }

    /**
     * Save selected machine
     *
     * @param Create $subject
     * @param Create $result
     * @return Create
     */
    public function afterSetShippingMethod(Create $subject, Create $result)
    {
        if (
            $subject->getShippingAddress()->getShippingMethod() === Carrier::CARRIER_CODE . '_' . Carrier::METHOD_CODE
            && $this->selectedMachine
        ) {
            $subject->getShippingAddress()->setInpostMachine($this->selectedMachine);
        }
        return $result;
    }

}