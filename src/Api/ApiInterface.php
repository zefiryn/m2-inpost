<?php

namespace Zefiryn\InPost\Api;

/**
 * API Interface
 *
 * @package Zefiryn\InPost\Api
 * @author Artur Jewuła <zefiryn@jewula.net>
 */
interface ApiInterface
{
    /**
     * Make request call to InPost API
     *
     * @param null $operation
     * @param array $params
     * @return mixed
     */
    public function performRequest($operation = null, $params = array());
}