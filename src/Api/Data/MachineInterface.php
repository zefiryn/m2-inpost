<?php

namespace Zefiryn\InPost\Api\Data;

/**
 * Machine Interface
 * @package Zefiryn\InPost\Api\Api
 * @author Artur Jewuła <zefiryn@jewula.net>
 */
interface MachineInterface
{
    const NAME = 'name';
    const POSTCODE = 'postcode';
    const STREET = 'street';
    const BUILDING_NUMBER = 'buildingnumber';
    const TOWN = 'town';
    const LATITUDE = 'latitude';
    const LONGITUDE = 'longitude';
    const DISTANCE = 'distance';
    const PAYMENT_TYPE = 'paymenttype';
    const LOCATION_DESCRIPTION = 'locationdescription';

    /**
     * @return string
     */
    public function getName();

    /**
     * @return string
     */
    public function getLocation();

    /**
     * @return string
     */
    public function getPostcode();

    /**
     * @return string
     */
    public function getStreet();

    /**
     * @return string
     */
    public function getBuildingNumber();

    /**
     * @return string
     */
    public function getTown();

    /**
     * @return float
     */
    public function getLatitude();

    /**
     * @return float
     */
    public function getLongitude();

    /**
     * @return float
     */
    public function getDistance();

    /**
     * @return string
     */
    public function getPaymentType();

    /**
     * @return string
     */
    public function getLocationDescription();
}