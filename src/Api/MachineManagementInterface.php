<?php
namespace Zefiryn\InPost\Api;

/**
 * Magento REST Endpoint Interface
 * @package Zefiryn\InPost\Api
 * @author Artur Jewuła <zefiryn@jewula.net>
 */
interface MachineManagementInterface
{

    /**
     * Find machines for the customer
     *
     * @param string $postcode
     * @param int $limit
     * @return \Zefiryn\InPost\Api\Data\MachineInterface[]
     */
    public function fetchNearestMachines($postcode, $limit);
}