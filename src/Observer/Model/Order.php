<?php

namespace Zefiryn\InPost\Observer\Model;

use Magento\Framework\Event\Observer;
use Magento\Framework\Event\ObserverInterface;
use Magento\Quote\Model\Quote;
use Magento\Sales\Api\Data\OrderInterface;
use Zefiryn\InPost\Model\Carrier;

class Order implements ObserverInterface
{

    public function execute(Observer $observer)
    {
        /** @var Quote $quote */
        $quote = $observer->getEvent()->getQuote();
        if ($quote->getShippingAddress()->getShippingMethod() == Carrier::CARRIER_CODE.'_'.Carrier::METHOD_CODE) {
            /** @var OrderInterface $order */
            $order = $observer->getEvent()->getOrder();
            $order->setInpostMachine($quote->getShippingAddress()->getInpostMachine());
            $order->setShippingDescription($order->getShippingDescription() . '(' . $quote->getShippingAddress()->getInpostMachine() . ')');
        }
    }
}