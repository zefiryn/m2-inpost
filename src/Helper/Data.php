<?php

namespace Zefiryn\InPost\Helper;

use Magento\Framework\App\Helper\AbstractHelper;
use Magento\Framework\App\Helper\Context;
use Zefiryn\InPost\Model\Config\Source\Selection;

/**
 * Class Data
 * @package Zefiryn\InPost\Helper
 * @author Artur Jewuła <zefiryn@jewula.net>
 */
class Data extends AbstractHelper
{
    const CONFIG_GOOGLE_API_KEY = 'carriers/inpost/google_api_key';
    const CONFIG_LEAFLET_PROVIDER = 'carriers/inpost/leaflet_provider';

    /**
     * @var Selection
     */
    protected $selectionSource;

    /**
     * Data constructor.
     *
     * @param Selection $selectionSource
     * @param Context $context
     */
    public function __construct(Selection $selectionSource, Context $context)
    {
        $this->selectionSource = $selectionSource;
        parent::__construct($context);
    }

    /**
     * @return integer
     */
    public function getMapType()
    {
        return $this->scopeConfig->getValue(Selection::CONFIG_PATH);
    }

    /**
     * @return string|null
     */
    public function getMapTypeAsString()
    {
        $selectedType = $this->getMapType();
        $labels = $this->selectionSource->toLabelArray();

        return array_key_exists($selectedType, $labels) ? $labels[$selectedType] : null;
    }

    /**
     * @return bool
     */
    public function useGoogleMaps()
    {
        return $this->getMapType() == Selection::GOOGLE;
    }

    public function getGoogleApiKey()
    {
        return $this->scopeConfig->getValue(self::CONFIG_GOOGLE_API_KEY);
    }

    /**
     * @return bool
     */
    public function useOpenLayersMaps()
    {
        return $this->getMapType() == Selection::OPEN_LAYERS;
    }

    /**
     * @return bool
     */
    public function useLeafletMaps()
    {
        return $this->getMapType() == Selection::LEAFLET;
    }

    public function getLeafletProvider()
    {
        return $this->scopeConfig->getValue(self::CONFIG_LEAFLET_PROVIDER);
    }
}