<?php

namespace Zefiryn\InPost\Block;

use Magento\Framework\View\Element\Template;
use Zefiryn\InPost\Helper\Data;

/**
 * Class Script
 * @package Zefiryn\InPost\Block
 * @author Artur Jewuła <zefiryn@jewula.net>
 */
class Script extends Template
{
    /**
     * @var Data
     */
    protected $helper;

    /**
     * Script constructor.
     * @param Data $helper
     * @param Template\Context $context
     * @param array $data
     */
    public function __construct(
        Data $helper,
        Template\Context $context,
        array $data = [])
    {
        $this->helper = $helper;
        parent::__construct($context, $data);
    }

    /**
     * @return mixed
     */
    public function getMapType()
    {
        return $this->helper->getMapType();
    }

    /**
     * @return bool
     */
    public function useGoogleMaps()
    {
        return $this->helper->useGoogleMaps();
    }

    /**
     * @return bool
     */
    public function getGoogleMapKey()
    {
        return $this->_scopeConfig->getValue('carriers/inpost/google_api_key');
    }

    public function getGoogleApiKey()
    {
        return $this->helper->getGoogleApiKey();
    }

    /**
     * @return bool
     */
    public function useOpenLayersMaps()
    {
        return $this->helper->useOpenLayersMaps();
    }

    /**
     * @return bool
     */
    public function useLeafletMaps()
    {
        return $this->helper->useLeafletMaps();
    }
}