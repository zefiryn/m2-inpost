<?php

namespace Zefiryn\InPost\Block\Adminhtml\Order\Create\Shipping\Method;

use Magento\Framework\Pricing\PriceCurrencyInterface;
use Magento\Sales\Block\Adminhtml\Order\Create\AbstractCreate;
use Magento\Sales\Model\AdminOrder\Create;
use Magento\Backend\Block\Template\Context;
use Magento\Backend\Model\Session\Quote;
use Psr\Log\LoggerInterface;
use Zefiryn\InPost\Api\Data\MachineInterface;
use Zefiryn\InPost\Model\Api;


class Form extends AbstractCreate
{
    /**
     * @var Api
     */
    protected $api;
    /**
     * @var LoggerInterface
     */
    protected $logger;

    protected $prefixes = '/^os\.|al\.|pl\.|ul\. /i';

    protected $replaceChars = [['ą','ę','ż','ź','ć','ś','ł','ó','ń'], ['a','e','z','z','c','s','l','o','n']];

    /**
     * Form constructor.
     * @param Context $context
     * @param Quote $sessionQuote
     * @param Create $orderCreate
     * @param PriceCurrencyInterface $priceCurrency
     * @param Api $api
     * @param LoggerInterface $logger
     * @param array $data
     */
    public function __construct(
        Context $context,
        Quote $sessionQuote,
        Create $orderCreate,
        PriceCurrencyInterface $priceCurrency,
        Api $api,
        LoggerInterface $logger,
        array $data = []
    ) {
        $this->api = $api;
        $this->logger = $logger;
        parent::__construct($context, $sessionQuote, $orderCreate, $priceCurrency, $data);
    }

    public function getMachines()
    {
        $result = $this->api->performRequest('findnearestmachines', ['postcode' => $this->getPostCode(), 'limit' => 100]);
        usort($result, function(MachineInterface $a, MachineInterface $b){
            $locationA = str_replace($this->replaceChars[0], $this->replaceChars[1], mb_strtolower(trim(preg_replace($this->prefixes, '', $a->getLocation()))));
            $locationB = str_replace($this->replaceChars[0], $this->replaceChars[1], mb_strtolower(trim(preg_replace($this->prefixes, '', $b->getLocation()))));
            return strcmp($locationA, $locationB) < 0 ? -1 : 1;
        });

        return $result;
    }

    public function getPostCode()
    {
        return $this->getQuote()->getShippingAddress()->getPostcode();
    }
}