<?php

namespace Zefiryn\InPost\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\SchemaSetupInterface;
use Magento\Framework\Setup\ModuleContextInterface;

class InstallSchema implements InstallSchemaInterface
{
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        $setup->startSetup();
        $setup->getConnection()->addColumn($setup->getTable('quote_address'), 'inpost_machine', [
            'type'      => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            'length'    => 255,
            'nullable'  => true,
            'comment'   => 'Selected InPost machine',
            'after'     => 'shipping_description'
        ]);
        $setup->getConnection()->addColumn($setup->getTable('sales_order'), 'inpost_machine', [
            'type'      => \Magento\Framework\DB\Ddl\Table::TYPE_TEXT,
            'length'    => 255,
            'nullable'  => true,
            'comment'   => 'Selected InPost machine',
            'after'     => 'shipping_method'
        ]);
        $setup->endSetup();
    }
}